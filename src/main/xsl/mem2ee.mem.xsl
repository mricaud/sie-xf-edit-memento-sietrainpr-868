<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
  xmlns:eflMem="http://www.lefebvre-sarrut.eu/ns/efl/memento"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  version="3.0">
  
  <xsl:import href="file:/D:/_WORK/_bitbucket-clean/SIE-XF-XDEV-FLASH/modules/sie-xf-sas-conversion/src/main/xsl/efl/memento_bu/mem2ee.mem.xsl"/>
  
  <xsl:param name="xfSAS:mainProduct" select="/processing-instruction('mainProduct')" as="xs:string" /> <!--EFL_GRP_mementoFiscal-->
  <!--Millesime : select en majuscule si on part d'un fichier BU ou en minuscule si om-sauv-->
  <xsl:param name="xfSAS:efl-mementoBu2editorialEntity.millesime" select="(/DYNAMEM/MEM/@MILLE, /dynamem/mem/@mille)[1]" as="xs:string"/>
  <!--FIXME : On va ignorer ces contentNode : il faudra statuer en atelier ce qu'on en fait-->
  <xsl:param name="xfSAS:efl-mementoBu2editorialEntity.keepTreeContentNode" select="false()" as="xs:boolean" />
  <!--Si on conserve les contentNode dans l'arbre on va tenter de les déplacer dans le EE pnum-->
  <xsl:param name="xfSAS:efl-mementoBu2editorialEntity.tryTomoveTreeContentNodeToPnumEE" select="true()" as="xs:boolean" />
  <!--Param local-->
  <xsl:param name="filterID" select="/processing-instruction('filterID')"/>
  
  <!--====================================================================================-->
  <!--INIT-->
  <!--====================================================================================-->
  
  <xsl:template match="/">
    <xsl:variable name="step" select="." as="document-node()"/>
    <xsl:variable name="step" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step"  mode="poc-modulo-lowercase"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step"  mode="poc-modulo-adapt-xml"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step"  mode="xfSAS:efl-mementoBu2editorialEntity.main"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step"  mode="poc-modulo-adapt-ee"/>
      </xsl:document>
    </xsl:variable>
    <xsl:sequence select="$step"/>
  </xsl:template>
 
  <!--====================================================================================-->
  <!--poc-modulo-lowercase-->
  <!--====================================================================================-->
  <!--Si le fichier vient de om-sauv, cette étape n'est pas nécessaire car on est déjà en XML minusculisé
  Mais si on vient d'un autre dossier (BU notamment) alors il faut convertir le SGML=>XML qui conserve les majuscules-->
  
  <xsl:mode name="poc-modulo-lowercase" on-no-match="shallow-copy"/>
  
  <xsl:template match="/*" mode="poc-modulo-lowercase">
    <xsl:choose>
      <!--Si la racine est déjà en misuscule on ne fait rien-->
      <xsl:when test="lower-case(name()) = name()">
        <xsl:sequence select="."/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:next-match/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="*" mode="poc-modulo-lowercase">
    <xsl:element name="{lower-case(name())}">
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="@*" mode="poc-modulo-lowercase">
    <xsl:attribute name="{lower-case(name())}" select="."/>
  </xsl:template>
  
  <xsl:template match="@xsi:*" mode="poc-modulo-lowercase">
    <xsl:copy-of select="."/>
  </xsl:template>
  
  <!--====================================================================================-->
  <!--poc-modulo-adapt-xml-->
  <!--====================================================================================-->
  
  <xsl:mode name="poc-modulo-adapt-xml" on-no-match="shallow-copy"/>
  
  <!--Suppression racine dynamem : xfSAS:efl-mementoBu2editorialEntity.main attends directement un <mem>-->
  <xsl:template match="/dynamem" mode="poc-modulo-adapt-xml">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!--Suppresion des ua/ur-->
  <xsl:template match="debua|debur|finua|finur" mode="poc-modulo-adapt-xml"/>
  
  <!--Suppression des actualisation en provenance des tcb (INNEO)
  => tous les P qui contiennent des TTCBD|TTCBE|TTCBF-->
  <xsl:template match="p[.//*[name() = ('ttcbd', 'ttcbe', 'ttcbf')]]" mode="poc-modulo-adapt-xml"/>
  
  <!--Suppression des liens CI INNEO-->
  <xsl:template match="link[contains(@linkid, 'EFL_CI')]" mode="poc-modulo-adapt-xml"/>
  
  <!--<link linkid="P2341E3C35A09CCC-EFL" col="BU" supp="MF" file="mf.sgm">78300</link> => rMemM-->
  
  <!--Element invalides trouvés dans MF 2019 version BU-->
  <xsl:template match="picto" mode="poc-modulo-adapt-xml"/>
  
  <!--====================================================================================-->
  <!--ADAPT-EE-->
  <!--====================================================================================-->
  
  <xsl:mode name="poc-modulo-adapt-ee" on-no-match="shallow-copy"/>
  
  <!--Filtrage demandé sur section 6-->
  <xsl:template match="xf:referenceNode[$filterID != ''][not(ancestor::xf:structuralNode[@xf:id = $filterID ])]" mode="poc-modulo-adapt-ee"/>
  
  <!--SIETRAINPR-868 : ajout CHAPITRE et SECTION devant introducteur de structuralNode (pour avoir un TOC iso(front dans Flash)-->
  
  <xsl:template match="xf:structuralNode[@type = 'CHAPITRE']/@num" mode="poc-modulo-adapt-ee">
    <xsl:attribute name="num" select="'CHAPITRE ' || ."/>
  </xsl:template>
  
  <xsl:template match="xf:structuralNode[@type = 'SECTION']/@num" mode="poc-modulo-adapt-ee">
    <xsl:attribute name="num" select="'SECTION ' || ."/>
  </xsl:template>
  
  <!--Suppression du div en trop : s'affiche mal avec le before en prévisu
  <title>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <div class="th">Autres corrections</div>
    </div>
  </title>-->
  <xsl:template match="xf:structuralNode/xf:title/*:div/*:div" mode="poc-modulo-adapt-ee">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!--<xsl:template match="xf:relation/xf:ref/@xf:idref" mode="poc-modulo-adapt-ee">
    <xsl:attribute name="xf:targetResId" select="."/>
  </xsl:template>
  
  <xsl:template match="xf:relation/@type" mode="poc-modulo-adapt-ee">
    <xsl:attribute name="calculated" select="'true'"/>
  </xsl:template>-->
  
</xsl:stylesheet>